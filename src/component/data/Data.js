import { sortNumber, sortString } from './Sorting.js';

function paginate(array, index, size) {
        // transform values
        index = Math.abs(parseInt(index));
        index = index > 0 ? index - 1 : index;
        size = parseInt(size);
        size = size < 1 ? 1 : size;

        // filter
        return [...(array.filter((value, n) => {
            return (n >= (index * size)) && (n < ((index+1) * size))
        }))]
    }

export function getOurservice(page) {
	let d = paginate(generateOurservice(), page, 3);
	return new Promise((resolve, reject) => {
    setTimeout(function() {
      resolve(d);
    }, 500);
  });
}

export function getOurserviceAll() {
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      resolve(generateOurservice());
    }, 500);
  });
}


function generateOurservice() {

	let content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem \
	ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor \
	incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, \
	consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

	return [
		{
			sort : 1,
			title : 'Thermal Temperature Detection',
			content : content,
			image : './image/service-1.jpg',

		},
		{
			sort : 2,
			title : 'Accurate People Counting',
			content : content,
			image : './image/service-2.jpg',

		},
		{
			sort : 3,
			title : 'Protect Area and Detect Masks',
			content : content,
			image : './image/service-3.jpg',

		},
		{
			sort : 4,
			title : 'Thermal Temperature Detection 2',
			content : content,
			image : './image/service-3.jpg',

		},
		{
			sort : 5,
			title : 'Accurate People Counting 2',
			content : content,
			image : './image/service-1.jpg',

		},
		{
			sort : 6,
			title : 'Protect Area and Detect Masks 2',
			content : content,
			image : './image/service-2.jpg',

		},
		{
			sort : 7,
			title : 'Protect Area and Detect Masks : page 3',
			content : content,
			image : './image/service-2.jpg',

		}

	];
}